import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { IBrokerConsumer, HttpGenericFilter } from 'bank-shared-lib';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const consumer = app.select(AppModule).get<IBrokerConsumer>('SagaExecutor');
  consumer.consume();
  app.useGlobalFilters(new HttpGenericFilter({}));
  await app.listen(3001);
}
bootstrap();
