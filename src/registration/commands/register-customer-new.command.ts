import { ConfigService } from '@nestjs/config';
import { BaseCommand } from 'bank-shared-lib';

export class RegisterCustomerNewCommand extends BaseCommand {
    constructor(private readonly configService: ConfigService) {
        super();
    }

    getCommandName(): string {
        return 'createCustomerCommand';
    }
    
    getCommandData(data: any) {
        const branchCode = this.configService.get<string>('branch.clientCode');
        data.customer.branchCode = branchCode;

        return data;
    }
}