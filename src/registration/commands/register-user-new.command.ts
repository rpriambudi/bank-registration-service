import { BaseCommand } from 'bank-shared-lib';

export class RegisterUserNewCommand extends BaseCommand {
    getCommandName(): string {
        return 'createUserCommand';
    }
}