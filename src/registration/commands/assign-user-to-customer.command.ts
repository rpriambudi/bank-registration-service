import { BaseCommand } from 'bank-shared-lib';

export class AssignUserToCustomerCommand extends BaseCommand {
    getCommandName(): string {
        return 'assignUserToCustomerCommand';
    }
}