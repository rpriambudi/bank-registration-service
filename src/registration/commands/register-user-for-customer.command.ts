import { BaseCommand } from 'bank-shared-lib';

export class RegisterUserForCustomerCommand extends BaseCommand {
    getCommandName(): string {
        return 'registerUserForCustomerCommand';
    }

}