import { UserRegistrationDto } from './../dto/user-registration.dto';
import { BaseCommand } from 'bank-shared-lib';

export class StartRegistrationCommand extends BaseCommand {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto
    ) {
        super();
    }
    
    getCommandName(): string {
        return 'startRegistrationCommand';
    }
    getCommandData() {
        return this.userRegistrationDto;
    }
}