import { BaseCommand } from 'bank-shared-lib';

export class AssignCustomerToUserNewCommand extends BaseCommand {
    getCommandName(): string {
        return 'assignCustomerToUserCommand';
    }
}