import { BaseCommand } from 'bank-shared-lib';
import { UserRegistrationDto } from './../dto/user-registration.dto';

export class StartExistingCustomerRegistrationCommand extends BaseCommand {
    constructor(
        public readonly userRegistrationDto: UserRegistrationDto
    ) {
        super();
    }

    getCommandName(): string {
        return 'startExistingCustomerRegistrationCommand';
    }

    getCommandData() {
        return this.userRegistrationDto;
    }
}