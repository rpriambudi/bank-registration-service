import { BaseCommand } from 'bank-shared-lib';

export class FinishRegistrationNewCommand extends BaseCommand {
    getCommandName(): string {
        return 'registrationFinished';
    }
}