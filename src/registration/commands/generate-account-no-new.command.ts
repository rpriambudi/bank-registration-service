import { ConfigService } from '@nestjs/config';
import { BaseCommand } from 'bank-shared-lib';

export class GenerateAccountNoNewCommand extends BaseCommand {
    constructor(private readonly configService: ConfigService) {
        super();
    }

    getCommandName(): string {
        return 'generateAccountNoCommand';
    }
    
    getCommandData(data: any) {
        const branchCode = this.configService.get<string>('branch.clientCode');
        return { branchCode: branchCode};
    }
}