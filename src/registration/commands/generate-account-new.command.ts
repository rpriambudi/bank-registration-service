import { ConfigService } from '@nestjs/config';
import { BaseCommand } from 'bank-shared-lib';

export class GenerateAccountNewCommand extends BaseCommand {
    constructor(private readonly configService: ConfigService) {
        super();
    }

    getCommandName(): string {
        return 'generateAccountCommand';
    }

    getCommandData(data: any) {
        const branchCode = this.configService.get<string>('branch.clientCode');
        return {
            branchCode: branchCode,
            customerId: data.customerId,
            accountNo: data.accountNo
        }
    }
}