import { BaseSaga, BaseEvent } from 'bank-shared-lib';
import { ExistingCustomerRegistrationStartedEvent } from './../events/existing-customer-registration-started.event';
import { UserCreatedForCustomerEvent } from './../events/user-created-for-customer.event';
import { AssignUserToCustomerCommand } from './../commands/assign-user-to-customer.command';
import { RegisterUserForCustomerCommand } from './../commands/register-user-for-customer.command';
import { UserAssignedToCustomerEvent } from './../events/user-assigned-to-customer.event';

export class ExistingCustomerRegistrationNewSaga extends BaseSaga {
    getEvents(): BaseEvent[] {
        return [
            new ExistingCustomerRegistrationStartedEvent(new RegisterUserForCustomerCommand()),
            new UserCreatedForCustomerEvent(new AssignUserToCustomerCommand()),
            new UserAssignedToCustomerEvent(null)
        ]
    }
}