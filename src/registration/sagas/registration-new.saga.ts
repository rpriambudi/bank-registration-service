import { Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { BaseSaga, BaseEvent, IBrokerPublisher, SagaState } from 'bank-shared-lib';
import { UserCreatedNewEvent } from '../events/user-created-new.event';
import { RegistrationStartedNewEvent } from '../events/registration-started-new.event';
import { CifNoGeneratedNewEvent } from '../events/cif-no-generated-new.event';
import { CustomerCreatedNewEvent } from '../events/customer-created-new.event';
import { CustomerAssignedToUserNewEvent } from '../events/customer-assigned-to-user-new.event';
import { AccountNoGeneratedNewEvent } from '../events/account-no-generated-new.event';
import { AccountGeneratedNewEvent } from '../events/account-generated-new.event';
import { GenerateCifNoNewCommand } from '../commands/generate-cif-no-new.command';
import { RegisterUserNewCommand } from '../commands/register-user-new.command';
import { RegisterCustomerNewCommand } from '../commands/register-customer-new.command';
import { AssignCustomerToUserNewCommand } from '../commands/assign-customer-to-user-new.command';
import { GenerateAccountNoNewCommand } from '../commands/generate-account-no-new.command';
import { GenerateAccountNewCommand } from '../commands/generate-account-new.command';
import { CreateUserFailedEvent } from '../events/fails/create-user-failed.event';
import { GenerateCifNoFailedEvent } from '../events/fails/generate-cif-no-failed.event';
import { CreateCustomerFailedEvent } from '../events/fails/create-customer-failed.event';
import { GenerateAccountNoFailedEvent } from '../events/fails/generate-account-no-failed.event';
import { GenerateAccountFailedEvent } from '../events/fails/generate-account-failed.event';
import { RegisterUserForCustomerFailedEvent } from '../events/fails/register-user-for-customer-failed.event';

export class RegistrationNewSaga extends BaseSaga {
    constructor(
        @InjectRepository(SagaState) private readonly sagaRepository: Repository<SagaState>,
        @Inject('BrokerPublisher') private readonly brokerPublisherIntf: IBrokerPublisher,
        @Inject(ConfigService) private readonly configService: ConfigService
    ) {
        super(sagaRepository, brokerPublisherIntf);
    }

    getEvents(): BaseEvent[] {
        return [
            new RegistrationStartedNewEvent(new RegisterUserNewCommand()),
            new UserCreatedNewEvent(new GenerateCifNoNewCommand(this.configService)),
            new CifNoGeneratedNewEvent(new RegisterCustomerNewCommand(this.configService)),
            new CustomerCreatedNewEvent(new AssignCustomerToUserNewCommand()),
            new CustomerAssignedToUserNewEvent(new GenerateAccountNoNewCommand(this.configService)),
            new AccountNoGeneratedNewEvent(new GenerateAccountNewCommand(this.configService)),
            new AccountGeneratedNewEvent(null),
            new CreateUserFailedEvent(null),
            new GenerateCifNoFailedEvent(null),
            new CreateCustomerFailedEvent(null),
            new GenerateAccountNoFailedEvent(null),
            new GenerateAccountFailedEvent(null),
            new RegisterUserForCustomerFailedEvent(null)
        ];
    }
}