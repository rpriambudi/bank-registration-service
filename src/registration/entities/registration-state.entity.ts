import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { UserRegistrationDto } from './../dto/user-registration.dto';

@Entity()
export class RegistrationState {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'int',
        name: 'user_id',
        nullable: true
    })
    userId: number;

    @Column({
        type: 'varchar',
        name: 'state'
    })
    state: string;

    @Column({
        type: 'jsonb',
        name: 'data'
    })
    data: UserRegistrationDto;

    @Column({
        type: 'varchar',
        name: 'description',
        nullable: true
    })
    description: string;
}

export enum StateTypes {
    RegistrationStart = 'REGISTRATION_STARTED',
    RegistrationFailed = 'REGISTRATION_FAILED',
    RegistrationFinished = 'REGISTRATION_FINISHED',
    UserClientCreated = 'USER_CLIENT_CREATED',
    UserClientFailed = 'USER_CLIENT_FAILED',
    GenerateCifNoFailed = 'GENERATE_CIF_NO_FAILED',
    CifNoGenerated = 'CIF_NO_GENERATED',
    RegisterCustomerFailed = 'REGISTER_CUSTOMER_FAILED',
    CustomerRegistered = 'CUSTOMER_REGISTERED',
    CustomerAssignedToUser = 'CUSTOMER_ASSIGNED_TO_USER',
    GenerateAccNoFailed = 'GENERATE_ACC_NO_FAILED',
    AccountNoGenerated = 'ACCOUNT_NO_GENERATED'
}