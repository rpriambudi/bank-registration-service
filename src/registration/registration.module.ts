import { Module } from "@nestjs/common";
import { RegistrationController } from './controllers/registration.controller';

@Module({
    imports: [],
    controllers: [RegistrationController]
})
export class RegistrationModule {}