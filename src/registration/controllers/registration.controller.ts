import axios from 'axios';
import { Controller, Get, Post, Body, Param, Inject } from '@nestjs/common';
import { SagaService } from 'bank-shared-lib';
import { ConfigService } from '@nestjs/config';
import { UserRegistrationDto } from './../dto/user-registration.dto';
import { RegistrationNewSaga } from './../sagas/registration-new.saga';
import { ExistingCustomerRegistrationNewSaga } from '../sagas/existing-customer-registration-new.saga';

@Controller()
export class RegistrationController {
    constructor(
        @Inject('RegistrationNewSaga') private readonly registrationSaga: RegistrationNewSaga,
        @Inject('ExistingCustomerRegistrationSaga') private readonly existingRegistrationSaga: ExistingCustomerRegistrationNewSaga,
        @Inject('SagaService') private readonly sagaService: SagaService,
        private readonly configService: ConfigService
    ) {}

    @Post('/api/registration')
    async registerNewCustomer(@Body() userRegistrationDto: UserRegistrationDto) {
        const customer = await this.getCustomerData(userRegistrationDto.customer.idType, userRegistrationDto.customer.idNumber);
        if (customer) {
            userRegistrationDto.customerId = customer.id;
            userRegistrationDto.cifNo = customer.cifNo;
            return await this.existingRegistrationSaga.startSaga(userRegistrationDto, 'existingCustomerRegistrationStartedEvent');
        }
        return await this.registrationSaga.startSaga(userRegistrationDto, 'registrationStartedEvent');
    }

    @Get('/api/registration/state/:stateId')
    async getRegistrationState(@Param('stateId') stateId: number) {
        return await this.sagaService.findSagaByStateId(stateId);
    } 

    private async getCustomerData(idType: string, idNumber: string): Promise<any> {
        const customerServiceUrl = this.configService.get<string>('customer.serviceUrl');
        const { data } = await axios.get(`${customerServiceUrl}/api/customer/${idType}/${idNumber}/idtype`);
        return data;
    }
}
