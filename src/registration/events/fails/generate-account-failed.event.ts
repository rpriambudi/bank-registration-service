import { BaseEvent } from 'bank-shared-lib';

export class GenerateAccountFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'generateAccountFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}