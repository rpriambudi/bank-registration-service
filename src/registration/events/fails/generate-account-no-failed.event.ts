import { BaseEvent } from 'bank-shared-lib';

export class GenerateAccountNoFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'generateAccountNoFailedEvent';
    }   
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}