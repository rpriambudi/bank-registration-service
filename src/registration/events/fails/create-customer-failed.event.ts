import { BaseEvent } from 'bank-shared-lib';

export class CreateCustomerFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'createCustomerFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
}