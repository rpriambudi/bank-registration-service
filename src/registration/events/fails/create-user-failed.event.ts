import { BaseEvent } from 'bank-shared-lib';

export class CreateUserFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'createUserFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }

}