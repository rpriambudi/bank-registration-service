import { BaseEvent } from 'bank-shared-lib';

export class RegisterUserForCustomerFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'registerUserForCustomerFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}