import { BaseEvent } from 'bank-shared-lib';

export class GenerateCifNoFailedEvent extends BaseEvent {
    getEventName(): string {
        return 'generateCifNoFailedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}