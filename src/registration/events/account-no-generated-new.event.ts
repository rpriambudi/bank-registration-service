import { BaseEvent } from 'bank-shared-lib';

export class AccountNoGeneratedNewEvent extends BaseEvent {
    getEventName(): string {
        return 'accountNoGeneratedEvent';
    }    

    getUpdatedData(stateData: any, eventData: any) {
        stateData.accountNo = eventData.accountNo;
        return stateData;
    }

}