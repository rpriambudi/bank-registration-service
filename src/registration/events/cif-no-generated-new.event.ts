import { BaseEvent } from 'bank-shared-lib';

export class CifNoGeneratedNewEvent extends BaseEvent {
    getEventName(): string {
        return 'cifNoGeneratedEvent';
    }

    getUpdatedData(stateData: any, eventData: any) {
        stateData.cifNo = eventData.cifNo;
        return stateData;
    }
    
}