import { BaseEvent } from 'bank-shared-lib';

export class CustomerAssignedToUserNewEvent extends BaseEvent {
    getEventName(): string {
        return 'customerAssignedToUserEvent';
    }    

    getUpdatedData(stateData: any, eventData: any) {
        stateData.accountNo = eventData.accountNo;
        return stateData;
    }


}