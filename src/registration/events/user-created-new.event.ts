import { ConfigService } from '@nestjs/config';
import { BaseEvent, BaseCommand } from 'bank-shared-lib';
import { GenerateCifNoNewCommand } from '../commands/generate-cif-no-new.command';

export class UserCreatedNewEvent extends BaseEvent {
    constructor(
        command: BaseCommand
    ) {
        super(command);
    }
    getEventName(): string {
        return 'userCreatedEvent';
    }    

    getUpdatedData(stateData: any, eventData: any) {
        stateData.userId = eventData.userId;
        return stateData;
    }
}