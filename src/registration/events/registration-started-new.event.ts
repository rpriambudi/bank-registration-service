import { BaseEvent, BaseCommand } from 'bank-shared-lib';
import { RegisterUserNewCommand } from './../commands/register-user-new.command';

export class RegistrationStartedNewEvent extends BaseEvent {
    getEventName(): string {
        return 'registrationStartedEvent';
    }

    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
}