import { BaseEvent } from 'bank-shared-lib';

export class CustomerCreatedNewEvent extends BaseEvent {
    getEventName(): string {
        return 'customerCreatedEvent';
    }    

    getUpdatedData(stateData: any, eventData: any) {
        stateData.customerId = eventData.customerId;
        return stateData;
    }
}