import { BaseEvent } from 'bank-shared-lib';

export class UserCreatedForCustomerEvent extends BaseEvent {
    getEventName(): string {
        return 'userCreatedForCustomerEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        stateData.userId = eventData.userId;
        return stateData;
    }

}