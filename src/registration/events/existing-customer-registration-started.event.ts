import { BaseEvent } from 'bank-shared-lib';

export class ExistingCustomerRegistrationStartedEvent extends BaseEvent{
    getEventName(): string {
        return 'existingCustomerRegistrationStartedEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
}