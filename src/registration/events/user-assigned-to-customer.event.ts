import { BaseEvent } from 'bank-shared-lib';

export class UserAssignedToCustomerEvent extends BaseEvent {
    getEventName(): string {
        return 'userAssignedToCustomerEvent';
    }    
    
    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }
    
}