import { BaseEvent, BaseCommand } from 'bank-shared-lib';
import { FinishRegistrationNewCommand } from '../commands/finish-registration-new.command';

export class AccountGeneratedNewEvent extends BaseEvent {
    getEventName(): string {
        return 'accountGeneratedEvent';
    }    
    
    getNextCommand(): BaseCommand {
        return null;
    }

    getUpdatedData(stateData: any, eventData: any) {
        return stateData;
    }

}