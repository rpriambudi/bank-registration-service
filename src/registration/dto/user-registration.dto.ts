import { IsNotEmpty } from 'class-validator';

export class UserRegistrationDto {
    @IsNotEmpty()
    user: any;

    @IsNotEmpty()
    customer: any;

    customerId: number;

    userId: number;

    accountNo: string;
    
    cifNo: string;

    branchCode: string;
}