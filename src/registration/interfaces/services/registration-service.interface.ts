import { RegistrationState } from './../../entities/registration-state.entity';

export interface RegistrationService {
    findRegistrationState(stateId: number): Promise<RegistrationState>;
}