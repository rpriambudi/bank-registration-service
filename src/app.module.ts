import { Module, Global } from '@nestjs/common';
import { TypeOrmModule, getRepositoryToken } from '@nestjs/typeorm';
import { ConfigModule, ConfigService} from '@nestjs/config';
import { BankSharedModule, SagaState } from 'bank-shared-lib'
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseConfig, BrokerConfig, SystemConfig, ServiceConfig } from './configs/configuration';
import { RegistrationModule } from './registration/registration.module';
import { RegistrationNewSaga } from './registration/sagas/registration-new.saga';
import { ExistingCustomerRegistrationNewSaga } from './registration/sagas/existing-customer-registration-new.saga';

@Global()
@Module({
  imports: [
    BankSharedModule.registerSaga({
      useFactory: (repository, brokerPublisher, configService: ConfigService) => {
        return {
          sagaList: [
            new RegistrationNewSaga(repository, brokerPublisher, configService),
            new ExistingCustomerRegistrationNewSaga(repository, brokerPublisher)
          ]
        }
      },
      inject: [getRepositoryToken(SagaState), 'BrokerPublisher', ConfigService]
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get<string>('database.host'),
          port: configService.get<number>('database.port'),
          username: configService.get<string>('database.username'),
          password: configService.get<string>('database.password'),
          database: configService.get<string>('database.name'),
          entities: [SagaState],
          synchronize: true,
          autoLoadEntities: true
        }
      },
      inject: [ConfigService]
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [DatabaseConfig, BrokerConfig, SystemConfig, ServiceConfig]
    }),
    RegistrationModule
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: 'RegistrationNewSaga',
      useClass: RegistrationNewSaga
    },
    {
      provide: 'ExistingCustomerRegistrationSaga',
      useClass: ExistingCustomerRegistrationNewSaga
    }
  ],
  exports: [
    {
      provide: 'RegistrationNewSaga',
      useClass: RegistrationNewSaga
    },
    {
      provide: 'ExistingCustomerRegistrationSaga',
      useClass: ExistingCustomerRegistrationNewSaga
    }
  ]
})
export class AppModule {}
