export const DatabaseConfig = () => ({
    database: {
        type: process.env.DB_TYPE,
        host: process.env.DB_HOST,
        port: Number(process.env.DB_PORT),
        username: process.env.DB_USER,
        password: process.env.DB_PASS,
        name: process.env.DB_NAME
    }
})

export const BrokerConfig = () => ({
    broker: {
        host: process.env.BROKER_HOST
    }
})

export const SystemConfig = () => ({
    branch: {
        clientCode: process.env.BRANCH_CLIENT_CODE
    }
})

export const ServiceConfig = () => ({
    customer: {
        serviceUrl: process.env.CUSTOMER_SERVICE_URL
    }
})